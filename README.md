## 简介

NovalIDE是一款开源，跨平台，而且免费的国产多功能，轻便的Python IDE，大小才12M

- 有出色的语法高亮功能,支持多种语言,python,c/c++,html,javascript,xml,css等
- 自动检测，并加载Python解释器，允许用户自由添加删除解释器，并选择相应的解释器运行脚本
- 支持函数智能提示和代码自动完成
- 支持新建NovalIDE工程和从现有代码创建工程，新建工程类型将包括应用程序，Django,Flask,wxPython,Py2exe,Win32,GTK，控制台程序等
- Django,Flask,wxPython,Py2exe,Win32,GTK，控制台程序等。可以断点调试，
- 自动智能分析解释器系统路径下模块文件，并生成智能提示使用的数据文件
- 类VS风格的可停靠窗口，多文档切换模式
- 各种复杂的编辑功能，支持高级编辑功能
- 可以调试以及模拟真实环境的终端方式运行python脚本
- 自动模拟Python解释器环境，并内嵌解释器，不用安装任何python环境，即可运行python程序
- 可以断点调试，单步调试python代码，并能监视，查看变量以及堆栈变化，以及添加，删除，管理断点等
- 可以自由终止，重启以及运行调试环境
- 支持运行多个解释器版本，并在不同版本之间进行切换
- 支持中英文多个语言版本
- 灵活的高扩展性，提供开放式接口支持程序员开发自定义插件
- 强大的包管理器，通过pip一键式安装卸载Python包
- 支持python2.6,2.7版本以及python3.x版本,2.6以前版本未实测

官方网址：[http://www.novalide.com](http://www.novalide.com/)

相关文章: [对一款优秀国产Python IDE---NovalIDE的体验](https://my.oschina.net/u/3728672/blog/1817030)

[软件图标](http://www.novalide.com/media/images/logo.png) 

[软件截图](http://www.novalide.com/media/images/index/banner_01.png) 

## 编译

## 安装

## 功能

## 感谢