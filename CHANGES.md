Changelog
=========

Changes with latest version of NovalIDE
----------------------------------------------

Version 1.0.0 -------------2017-12-23
1.support Cross Platform editor
2.support HTML,python,text xml lanauge 
3.auto load default python interpreter
4.find replace text in open document
5.find text in directory files
6.convinient to install editor in windows and linux system
7.auto detect file modified or deleted without editor
8.support mru files and delete invalid file path
9.enable show tips when startup application
10.multi document tabbed frame style
11.auto detect python file encoding when save python document
12.enable code margin fold style
13.simple support debug or run a python script in editor or terminator 


Version 1.0.1 -------------2017-12-27
1.enable check python file syntax
2.show caret line background color
3.enbale open file path in terminator
4.set debug or run script execution dir is script path
5.support python script path contains chinese
6.optimise debug run mode
7.enable kill or stop debug run process
8.enable debug have stdin input python script
9.enable colorize error output or input text
10.enable terminate all running process
11.change application splash image
12.delete ide splash embed bitmap data
13.enable user choose one interpreter from multiple choices
14.auto analyse intepreter interllisence data
15.go to definition of text


Version 1.0.2 -------------2018-01-08
1.implement go to word definition in global scope search
2.interligent analyze class self property in class object method
3.remove duplicate child name of class object property
4.find definition in document scope
5.enable sort in outline service
6.enable custom context menu in debug output tab,and enable word wrap in line
7.can only launch one find or replace dialog instance
8.enable to add one interpreter to editor
9.add configure interpreter function
10.display interpreter sys path list ,builtin modules and environment
11.make install packages run in windows system
12.show smart analyse progress dialog
13.implement file resource view
14.enable find location in outline view
15.initial implement code completion
16.import and from import code completion
17.complete type object completion 


Version 1.0.3 -------------2018-01-16
1.smart show module and class member completion
2.recognize python base type object and show it's member info
3.set intellisense database version
4.set virtual scope with database members file
5.show progress dialog whhen find text in files
6.repair search text progress dialog bug
7.enable set script parameter and environment
8.support create unit test frame of script
9.repair py2exe call subprocess.popen error
10.repair cannot inspect cp936 encoding error
11.add insert and advance edit function
12.separate common intellisense data path and builtin intellisense data,which on linux system ,they are not equal
13.add builtin intellisense data to linux build package
14.enable insert date,comment template and python coding declare to text
15.enable gotodef and listmembers shortcut in menu item
16.default use right edge mode in text document
17.set xml and html parser fold style
18.support parse relative import 
19.repair relative import position bug
20.upgrade intellisense database generation and update database version to 1.0.2
21.support go to defition of from import modules and their childs
22.optimise memberlist sort algorithm


Version 1.0.4 -------------2018-02-04
1.repair show tip bug
2.repair open all files bug in linux system
3.auto analyse sys moudles intellisense data
4.improve and supplement chinese translation
5.smart anlalyse class base inherited classes members
6.analyse from import members
7.update install instructions on linux system of readme
8.allow user choose line eol mode
9.add icon to Python Interpreter and Search Result,Debug View
10.enable click plus button of dir tree to expand child item in resource view
11.supplement chinese translation of wx stock id items
12.repair bug when load interpreters and set first add interpreter as default interpreter
13.repair check syntax bug in python2.6
14.partly support smart analyse interpreter data of python3 version
15.optimise outline load parse data mechanism and repair load file bug
17.implement new auto completion function of menu item
18.fix package install bug on linux os
19.fix serious bug when load python3 interprter and smart analyse its data when convert to windows exe with py2exe
20.add debug mode which will be convert to console exe with py2exe
21.prevent pop warn dialog when the windows exe program exit which is converted by py2exe
22.fix 'cannot import name Publisher' error when use py2exe convert to windows exe
23.fix run python script which path has chinese character on linux os
24.close threading.Thread VERBOSE mode in debugger service
25.increase mru history file count from 9 to 20 and allow user to set the max MRU item count
26.fix right up menu of document position
27.fix bug when run script path contain chinese character on windows os


Version 1.0.5 -------------2018-02-13
1.enable to open current python interpreter on tools menu
2.allow user to set use MRU menu or not
3.implement navigation to next or previous position
4.analyse class static members intellisense data
5.analyse class and function doc declaration
6.simple implement project creation
7.analyse builtin member doc declaration and show its calltip information
8.separate builtin intellisense data to python 2 and 3
9.repair python3 parse function args bug
10.fix autoindent bug when enter key is pressed
11.Sets when a backspace pressed should do indentation unindents
12.modify project document definition ,add project name and interpreter to it 
13.fix the error of set progress max range when search text in dir 
14.enable set enviroment variable of interpreter when debug or run python script
15.fix output truncated error when debug python script 
16.add function and class argment show tip
17.fix application exit bug when interpreter has been uninstalled
18.fix the bug of subprocess.popen run error on some computer cause application cann't startup
19.set calltip background and foreground color
20.fix the bug when debug run python window program the window interface is hidden
21.show python interpreter pip package list in configuration
22.disable or enable back delete key when debug output has input according to the caret pos
23.when load interpreter configuation list,at least select one interprter in datalist view control
24.fix get python3 release version error,which is alpha,beta,candidate and final version

Version 1.0.6 -------------2018-03-6
1.add drive icon and volument name on resource view
2.use multithread to load python pip package
3.fix image view display context menu bug
4.change python and text file icon
5.add project file icon to project and show project root
6.show local icon of file when scan disk file on resource view
7.when add point and brace symbol to document text will delete selected text
8.save folder open state when swith toggle button on resource view
9.parse main function node and show main function in outline view
10.fix parse file content bug where content encoding is error
11.fix parse collections package recursive bug in python3
12.when current interpreter is analysing,load intellisense data at end
13.try to get python help path in python loation if help path is empty on windows os
14.fix image size is not suitable which will invoke corruption on linux os.convernt png image to 16*16
15.fix the bug when click the first line of search results view
16.allow add or remove python search path,such as directory,zip ,egg and wheel file path
17.add new python virtual env function
18.fix environment variable contains unicode character bug
19.fix add python virtual env bug on linux os
20.fix the bug when double click the file line and show messagebox which cause wxEVT_MOUSE_CAPTURE_LOST problem
21.adjust the control layout of configuration page
22.remove the left gap if python interpreter view
23.allow load builtin python interpreter
24.enable debug file in python interpreter view with builtin interpreter
25.optimise the speed of load outline of parse tree
26.change startup splash image of application
27.fix bug when show startup splash image on linux os

Version 1.0.7 -------------2018-03-23
1.query the running process to stop or not when application exit
2.fix the bug when the interpreter path contain empty blank string
3.add icon of unittest treectrl item
4.add import files to project and show import file progress
5.beautify unittest wizard ui interface
6.add new project wizard on right menu of project view
7.fix the bug of import files on linux os
8.fix the coruption problem of debug run script with builtin interpreter
9.fix the bug of open file in file explower on windows os
10.optimise and fix bug of import project files
11.copy files to project dest dir when import files
12.fix the bug when load saved projects
13.show prompt message dialog when the import files to project have already exist
14.enable filter file types when import files to project 
15.fix getcwd coruption bug on linux os when currrent path is deleted
16.fix file observer bug on linux os when the watched path is deleted
17.add open project and save project memu item on project menu and implement the menu action
18.add monitor to currrent project to generate intellisense data of the project
19.add project icon of NovalIDE project file with the .nov file extension and associated with NovalIDE application
20.enable click on the .nov file to open NovalIDE project
21.enable add ,delete and manage breakpoints
22.enable add package folder in project
23.correct the error number count of search text result in project
24.enable open project from command line and set as current project
25.update nsis script to write project file extension and icon to regsitry
26.fix the serious bug of close and delete project
27.enable filter file types when import files to project
28.fix the bug of project file or path contains chinese character

Version 1.0.8 -------------2018-04-01
1.fix create virtualenv bug on linux os
2.fix the bug of file watcher when save as the same file
3.update english to chinese translation
4.replace toolbar old image with new image
5.fix a serious breakpoint debug bug
6.add icon to meneu item
7.enable install and uninstall package
8.add breakpoint debug menu item
9.show detail progress information of install and uninstall package
10.fix the bug of goto definition bug
11.fix the bug of kill process fail on linux os
12.kill the smart analyse processes when application exit and smart analyse is running
13.add project id attribute to project instance
14.enable mutiple selections on project files
15.delete project regkey config when close project or delete project
16.show styled text of binary document
17.show file encoding of document on status bar
18.save document cache position when close frame and load position data when open document
19.enable modify project name with label edit
20.set the startup script of project and bold the project startup item
21.enable run and debug last settings of project or file
22.fix the bug of create unittest when parse file error
23.fix the check syntax error of python script bug
24.save all open project documents before run
25.fix the bug of close all run tabs
26.fix the bug of save chinese text document
27.allow set the default file encoding of text document
28.fix the bug of pip path contains blank when install and uninstall package
29.fix the bug of close debugger window when application exit


Version 1.1.0 -------------2018-05-16
1.fix the bug of deepcopy run parameter iter attribute
2.create novalide web server project
3.enable check for app update version on menu item
4.enable download update version from web server
5.establish the simple novalide web server
6.enable delete files and folder from proeject and local file system
7.check update version info when application start up
8.support install package with pip promote to root user on linux os
9.set default logger debug level to info level
10.close open project files when delete project files or folders
11.support auto check ,download and install new version of app on linux os
12.enable rename folder and file item of project
13.when renane folder of project,rename the open folder files
14.watch the project document,and alarm the document change event
15.fix the bug of load python when app startup on some computer
16.fix the bug of null encoding when set file encoding
17.enable copy and cut files of project,and paste to another position
18.add csv module to library zip when use py2exe to pack
19.allow add python package folder to project folder item
20.fix a bug of load python interpreter from config
21.enable drag file items to move files of project to dest
22.fix the save as document bug whe the save as document filename is already opened